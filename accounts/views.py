from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginUser, SignUp


# Create your views here.
def login_user(request):
    if request.method == "POST":
        loginform = LoginUser(request.POST)
        if loginform.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                loginform.add_error("password", "Passwords do not match")

    elif request.method == "GET":
        loginform = LoginUser()

    context = {
        "user": loginform,
    }

    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def signup_user(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password,
                )
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUp()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
