from django.urls import path
from receipts.views import (
    show_receipts,
    create_receipt,
    category_overview,
    account_overview,
    create_category,
    create_account,
)

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_overview, name="category_list"),
    path("accounts/", account_overview, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
