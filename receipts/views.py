from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceipt, CreateCategory, CreateAccount

# Create your views here.


@login_required(redirect_field_name="/accounts/login")
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}
    return render(request, "receipts/receipts.html", context)


@login_required(redirect_field_name="/accounts/login")
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            new_receipt = form.save(False)
            new_receipt.purchaser = request.user
            new_receipt = form.save()
            return redirect("home")
    else:
        form = CreateReceipt()

    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required(redirect_field_name="/accounts/login")
def category_overview(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category_list}
    return render(request, "receipts/categories.html", context)


@login_required(redirect_field_name="/accounts/login")
def account_overview(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(request, "receipts/accounts.html", context)


@login_required(redirect_field_name="/accounts/login")
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            new_category = form.save()
            return redirect("category_list")
    else:
        form = CreateCategory()

    context = {"form": form}

    return render(request, "categories/create.html", context)


@login_required(redirect_field_name="/accounts/login")
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            new_account = form.save()
            return redirect("account_list")
    else:
        form = CreateAccount(request.POST)

    context = {"form": form}
    return render(request, "accounts/create.html", context)
